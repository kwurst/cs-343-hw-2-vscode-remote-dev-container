package cs_343.duck_simulator;

/**
 * Duck simulator based on Chapter 1 of Head First Design Patterns.
 * @author Karl R. Wurst
 * Copyright 2017 licensed under GPLv3
 *
 */
public class RubberDuck extends Duck {

	@Override
	public void quack() {
		System.out.println("Squeak");
	}
	
	@Override
	public void display() {
		System.out.println("I'm a Rubber Duck");
	}
	
	@Override
	public void fly() {
	}

}
