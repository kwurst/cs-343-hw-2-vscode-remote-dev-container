package cs_343.duck_simulator;

import java.util.ArrayList;

/**
 * Duck simulator based on Chapter 1 of Head First Design Patterns.
 * @author Karl R. Wurst
 * Copyright 2017 licensed under GPLv3
 *
 */
public class DuckSimulator {

	public static void main(String[] args) {
		ArrayList<Duck> pond = new ArrayList<Duck>();
		
		Duck newDuck;
		
		newDuck = new MallardDuck();
		pond.add(newDuck);
		
		newDuck = new RedHeadDuck();
		pond.add(newDuck);
		
		newDuck = new RubberDuck();
		pond.add(newDuck);
		
		newDuck = new DecoyDuck();
		pond.add(newDuck);
		
		for (Duck duck:pond) {
			duck.display();
			duck.swim();
			duck.quack();
			duck.fly();
		}
		
	}

}
