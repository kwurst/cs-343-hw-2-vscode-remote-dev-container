package cs_343.duck_simulator;

import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
import org.junit.After;

import static org.hamcrest.CoreMatchers.*;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

/**
 * Duck simulator based on Chapter 1 of Head First Design Patterns.
 * @author Karl R. Wurst
 * Copyright 2017 licensed under GPLv3
 *
 */
public class MallardDuckTest {
	
	private Duck d1;
	private PrintStream oldOut;
	private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
	private String newline = System.getProperty("line.separator");

	@Before
	public void setUp() {
		d1 = new MallardDuck();
		oldOut = System.out;
		System.setOut(new PrintStream(outContent));
	}

	@Test
	public void testQuack() {
		d1.quack();
		assertThat(outContent.toString(), is("Quack" + newline));
	}
	
	@Test
	public void testSwim() {
		d1.swim();
		assertThat(outContent.toString(), is("Swim, swim" + newline));
	}
	
	@Test
	public void testDisplay() {
		d1.display();
		assertThat(outContent.toString(), is("I'm a Mallard Duck" + newline));
	}
	
	@Test
	public void testFly() {
		d1.fly();
		assertThat(outContent.toString(), is("Flap, flap" + newline));
	}

	@After
	public void tearDown() {
		System.setOut(oldOut);
	}
}

