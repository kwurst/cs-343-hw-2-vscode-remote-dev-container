# Homework 2

**For the actual homework assignment, go to [HW2.md](HW2.md).**

This is a poorly designed project that needs to be refactored using the Strategy Pattern, the Singleton Pattern, and the Simple Factory Pattern.

It has tests, and they all pass.

To build and test with Gradle at the command line:

- Windows: `gradlew.bat build`
- Mac/Linux: `./gradlew build`

junk